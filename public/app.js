'use strict'

let
  find = query => document.querySelector(query),
  findAll = query => document.querySelectorAll(query);



(function(form) {

  let model = i => `<div><img src="img/форма${i}.jpg"></div>`
  let gallery = ''

  for(let i = 0; i < form; i++) gallery += model(i)

  find('div.form').innerHTML = gallery

})(6);
(function() {
  let
    photo = find('button.photo'),
    video = find('button.video'),
    form = find('button.form'),
    stadium = find('button.stadium'),
    prev

  function show() {
    if(prev) prev.classList.remove('visible')
    let target = this.className, elem = find('div.' + target)
    console.log(elem)
    elem.classList.toggle('visible')
    prev = elem
  }

  photo.onclick = show
  video.onclick = show
  form.onclick = show
  stadium.onclick = show
})();


(function() {
  let
    history = find('button.history'),
    comp = find('button.composition'),
    gallery = find('button.gallery'),
    prev

  function show() {
    if(prev) prev.classList.remove('visible')
    let target = this.className, elem = find('section.' + target)
    elem.classList.toggle('visible')
    prev = elem
  }

  history.onclick = show
  comp.onclick = show
  gallery.onclick = show
})();

(function(photo) {

  let model = i => `<div><img src="img/галерея${i}.jpg"></div>`
  let gallery = ''

  for(let i = 0; i < photo; i++) gallery += model(i)

  find('div.photo').innerHTML = gallery

})(78);
(function(stadiums) {

  let model = i => `<div><img src="img/стадион${i}.jpg"></div>`
  let gallery = ''

  for(let i = 0; i < stadiums; i++) gallery += model(i)

  find('div.stadium').innerHTML = gallery

})(5);
(function(team) {

  let
    a = find('button.bosses'),
    b = find('button.team'),
    prev

  function show() {
    if(prev) prev.classList.remove('visible')
    let target = this.className, elem = find('div.' + target)
    console.log(elem)
    elem.classList.toggle('visible')
    prev = elem
  }

  a.onclick = show
  b.onclick = show

  let model = memb => `
    <div>
      <img src="img/${memb.numb}.jpg">
      <h4>Номер</h4>
      <p>${memb.numb}</p>
      <h4>Гражданство</h4>
      <p>${memb.city}</p>
      <h4>Позиция</h4>
      <p>${memb.pos}</p>
      <h4>Имя</h4>
      <p>${memb.name}</p>
      <h4>Возраст</h4>
      <p>${memb.age}</p>
	  <div style="clear:both"></div>
    </div>`

  let collection = ''

  team.forEach(member => collection += model(member))

  find('div.team').innerHTML = collection

})([
  {numb: 1, city: 'Польша', pos: 'ВР', name: 'Войцех Щесный', age: 26},
  {numb: 2, city: 'Франция', pos: 'ЗАЩ', name: 'Матьё Дебюши', age: 31},
  {numb: 3, city: 'Англия', pos: 'ЗАЩ', name: 'Киран Гиббс', age: 27},
  {numb: 4, city: 'Германия', pos: 'ЗАЩ', name: 'Пер Мертезакер', age: 32},
  {numb: 5, city: 'Бразилия', pos: 'ЗАЩ', name: 'Габриэл Паулиста', age: 26},
  {numb: 6, city: 'Франция', pos: 'ЗАЩ', name: 'Лоран Косельни', age: 31},
  {numb: 7, city: 'Чили', pos: 'НАП', name: 'Алексис Санчес', age: 28},
  {numb: 8, city: 'Уэльс', pos: 'ПЗ', name: 'Аарон Ремзи', age: 26},
  {numb: 9, city: 'Испания', pos: 'НАП', name: 'Лукас Перес', age: 28},
  {numb: 10, city: 'Англия', pos: 'ПЗ', name: 'Джек Уилшир', age: 25},
  {numb: 11, city: 'Германия', pos: 'ПЗ', name: 'Месут Озил', age: 28}, 
  {numb: 12, city: 'Франция', pos: 'НАП', name: 'Оливье Жиру', age: 30},
  {numb: 13, city: 'Колумбия', pos: 'ВР', name: 'Давид Оспина', age: 28},
  {numb: 14, city: 'Англия', pos: 'НАП', name: 'Тео Уолкотт', age: 28},
  {numb: 15, city: 'Англия', pos: 'ПЗ', name: 'Алекс Окслейд-Чемберлен', age: 23},
  {numb: 16, city: 'Англия', pos: 'ЗАЩ', name: 'Роб Холдинг', age: 21},
  {numb: 17, city: 'Нигерия', pos: 'НАП', name: 'Алекс Ивоби', age: 20},
  {numb: 18, city: 'Испания', pos: 'ЗАЩ', name: 'Начо Монреаль', age: 31},
  {numb: 19, city: 'Испания', pos: 'ПЗ', name: 'Санти Касорла', age: 32},
  {numb: 20, city: 'Германия', pos: 'ЗАЩ', name: 'Шкодран Мустафи', age: 24},
  {numb: 21, city: 'Англия', pos: 'ЗАЩ', name: 'Калум Чамберс', age: 22},
  {numb: 22, city: 'Франция', pos: 'НАП', name: 'Яя Саного', age: 23},
  {numb: 23, city: 'Англия', pos: 'НАП', name: 'Данни Уэлбек', age: 26},
  {numb: 24, city: 'Испания', pos: 'ЗАЩ', name: 'Эктор Бельерин', age: 21},
  {numb: 25, city: 'Англия', pos: 'ЗАЩ', name: 'Карл Дженкинсон', age: 25},
  {numb: 26, city: 'Аргентина', pos: 'ВР', name: 'Дамиан Мартинес', age: 24},
  {numb: 28, city: 'Коста-Рика', pos: 'ПЗ', name: 'Хоэль Кэмпбелл', age: 24},
  {numb: 29, city: 'Швейцария', pos: 'ПЗ', name: 'Гранит Джака', age: 24},
  {numb: 31, city: 'Франция', pos: 'ПЗ', name: 'Жефф Рейн-Аделаид', age: 20},
  {numb: 32, city: 'Англия', pos: 'НАП', name: 'Чуба Акпом', age: 22},
  {numb: 33, city: 'Чехия', pos: 'ВР', name: 'Петр Чех', age: 34},
  {numb: 34, city: 'Франция', pos: 'ПЗ', name: 'Франсис Коклен', age: 25},
  {numb: 35, city: 'Египет', pos: 'ПЗ', name: 'Мохаммед эльНенни', age: 24},
  {numb: 55, city: 'Англия', pos: 'ПЗ', name: 'Эйнсли Мэйтленд-Найлз', age: 19},
  {numb: 68, city: 'Англия', pos: 'НАП', name: 'Кэлом Уиллок', age: 19}
]);